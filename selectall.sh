#!/usr/bin/env zsh

echo cp /pixelscratch/pixelscratch/config/BPix/amc13/0/amc13.dat_bpix /pixelscratch/pixelscratch/config/BPix/amc13/0/amc13.dat
echo cp /pixelscratch/pixelscratch/config/BPix/portcardmap/0/portcardmap.dat_bpix /pixelscratch/pixelscratch/config/BPix/portcardmap/0/portcardmap.dat
echo cp /pixelscratch/pixelscratch/config/BPix/nametranslation/0/translation.dat_bpix /pixelscratch/pixelscratch/config/BPix/nametranslation/0/translation.dat
echo cp /pixelscratch/pixelscratch/config/BPix/detconfig/0/detectconfig.dat_bpix /pixelscratch/pixelscratch/config/BPix/detconfig/0/detectconfig.dat

cp /pixelscratch/pixelscratch/config/BPix/amc13/0/amc13.dat_bpix /pixelscratch/pixelscratch/config/BPix/amc13/0/amc13.dat

cp /pixelscratch/pixelscratch/config/BPix/portcardmap/0/portcardmap.dat_bpix /pixelscratch/pixelscratch/config/BPix/portcardmap/0/portcardmap.dat

cp /pixelscratch/pixelscratch/config/BPix/nametranslation/0/translation.dat_bpix /pixelscratch/pixelscratch/config/BPix/nametranslation/0/translation.dat

cp /pixelscratch/pixelscratch/config/BPix/detconfig/0/detectconfig.dat_bpix /pixelscratch/pixelscratch/config/BPix/detconfig/0/detectconfig.dat
