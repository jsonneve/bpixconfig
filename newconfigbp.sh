#!/usr/bin/env zsh
# from Jory
# cc0 copyleft
# public domain

# Specify the quadrant, filename appendix (e.g. 1234 or nothing), AND
# sector to start with and sectors to loop over below (change 4 lines!)


# Specify the quadrant:

./newconfig.sh bpo
./newconfig.sh bpi

for settings in 'portcardmap' 'detectconfig' 'translation'
    do
        cardname=${settings}.dat_bp
        directory=${settings}/0/ # assuming version 0

        echo ${directory}${cardname}
        cat ${directory}${cardname}o >  ${directory}${cardname} # start with 1st specified sector to include any headers

        # now exclude all headers
        grep -v '#' ${directory}${cardname}i | grep -v -i 'Rocs'  >>  ${directory}${cardname}
    done

echo ''
echo Did not take into account amc! Sorry not yet implemented
echo Now do
echo ./selectshell.sh bp