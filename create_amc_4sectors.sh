#!/usr/bin/env zsh


grep ' 1 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bpi1234
grep ' 2 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bpo1234
grep ' 3 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bpi5678
grep ' 4 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bpo5678
grep ' 5 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bmi1234
grep ' 6 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bmo1234
grep ' 7 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bmi5678
grep ' 8 ' amc13/0/amc13.dat_withnumbers > amc13/0/amc13.dat_bmo5678