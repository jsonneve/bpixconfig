#!/usr/bin/env zsh



# Take SDA from portcard version 1
for card in portcard/1/*.dat
    do
        # Copy SDA lie
        myvar=`grep SDA $card`; echo $myvar
        # Paste into card in portcard version 3
        card2=portcard/7/$(basename $card)
        # Replace
        sed -i 's/Delay25_SDA.*/'$myvar'/' $card2
    done