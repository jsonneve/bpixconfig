#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
//#include <stdlib.h>

using namespace std;

int main() {

  ifstream infile; //input file, name data_file uniqe
  infile.open("old.dat",ios::in); // open data file

  ofstream outfile; //output file, name data_file uniqe
  outfile.open("new.dat",ios::trunc); // open data file


  cout << infile.eof() << " " << infile.bad() << " "
       << infile.fail() << " " << infile.good()<<endl;

  if (infile.fail()) {
    cout << " File not found " << endl;
    return(1); // signal error
  }

  //string line;
  char line[500];
  infile.getline(line,500,'\n');
  //infile>>line;
  cout<<line<<endl;
  outfile<<line<<endl;

  string name;
  int fec,mfec,mfecChan,hub,port,rocId,fed,fedChan,rocOrder;
  string tbm = "A";

  bool print = true;
  for(int i=0;i<100000;++i) {

    infile>>name>>tbm>>fec>>mfec>>mfecChan>>hub>>port>>rocId>>fed>>fedChan>>rocOrder;
    if(print) 
      cout<<name<<" "<<tbm<<" "<<fec<<"    "<<mfec<<"       "<<mfecChan<<"          "
	  <<hub<<"        "<<port<<"      "<<rocId<<"    "<<fed<<"     "
	  <<fedChan<<"     "<<rocOrder<<endl;

    if(name==" ")continue;

    if ( infile.eof() != 0 ) {
      cout<< " end of file " << endl;
      break;;
    } else if (infile.fail()) { // check for errors
      cout << "Cannot read data file" << endl;
      return(1);
    }

    bool inner = true;
    bool positive = true;
    string::size_type idx;
    idx = name.find("_BpI_");
    if(idx != string::npos) {inner = true; positive=true;}
    idx = name.find("_BpO_");
    if(idx != string::npos) {inner = false; positive=true;}
    idx = name.find("_BmI_");
    if(idx != string::npos) {inner = true; positive=false;}
    idx = name.find("_BmO_");
    if(idx != string::npos) {inner = false; positive=false;}
    
    int layer=0;
    idx = name.find("_LYR1_");
    if(idx != string::npos) {layer=1;}
    idx = name.find("_LYR2_");
    if(idx != string::npos) {layer=2;}
    idx = name.find("_LYR3_");
    if(idx != string::npos) {layer=3;}
    idx = name.find("_LYR4_");
    if(idx != string::npos) {layer=4;}
    
    int sector=0;
    idx = name.find("_SEC1_");
    if(idx != string::npos) {sector=1;}
    idx = name.find("_SEC2_");
    if(idx != string::npos) {sector=2;}
    idx = name.find("_SEC3_");
    if(idx != string::npos) {sector=3;}
    idx = name.find("_SEC4_");
    if(idx != string::npos) {sector=4;}
    idx = name.find("_SEC5_");
    if(idx != string::npos) {sector=5;}
    idx = name.find("_SEC6_");
    if(idx != string::npos) {sector=6;}
    idx = name.find("_SEC7_");
    if(idx != string::npos) {sector=7;}
    idx = name.find("_SEC8_");
    if(idx != string::npos) {sector=8;}
    
    // Do modifications
    // HUB address change 
    // if( (sector==1) || (sector==8) ) {
    // 	 if( (hub>=28) && (hub<=31) ) hub = hub-4;
    // 	 else if( (hub>=24) && (hub<=27) ) hub = hub+4;
    // } else if( (sector==2) || (sector==4) || (sector==6) ) {
    // 	 if( (hub>=28) && (hub<=31) ) hub = hub-4;
    // 	 else if( (hub>=24) && (hub<=27) ) hub = hub+4;
    // } else if( (sector==3) || (sector==5) || (sector==7) ) {
    // 	 if( (hub>=24) && (hub<=27) ) hub = hub-4;
    // 	 else if( (hub>=20) && (hub<=23) ) hub = hub+4;
    // }
    
    
    // chengs mfecs for sectors 5-8
    if(sector==5)      {if(mfec==1) mfec=4; else cout<<" error1 "<<endl;}
    else if(sector==6) {if(mfec==2) mfec=3; else cout<<" error2 "<<endl;}
    else if(sector==7) {if(mfec==3) mfec=2; else cout<<" error3 "<<endl;}
    else if(sector==8) {if(mfec==4) mfec=1; else cout<<" error4 "<<endl;}

    // Change fec and mfec
    // if(positive && inner) { // BpI

    //   if(sector==1)      {fec=1; mfec=1;}
    //   else if(sector==2) {fec=1; mfec=2;}
    //   else if(sector==3) {fec=1; mfec=3;}
    //   else if(sector==4) {fec=1; mfec=4;}
    //   else if(sector==5) {fec=3; mfec=1;}
    //   else if(sector==6) {fec=3; mfec=2;}
    //   else if(sector==7) {fec=3; mfec=3;}
    //   else if(sector==8) {fec=3; mfec=4;}

    // } else if(positive && !inner) { // BpO

    //   if(sector==1)      {fec=2; mfec=1;}
    //   else if(sector==2) {fec=2; mfec=2;}
    //   else if(sector==3) {fec=2; mfec=3;}
    //   else if(sector==4) {fec=2; mfec=4;}
    //   else if(sector==5) {fec=4; mfec=1;}
    //   else if(sector==6) {fec=4; mfec=2;}
    //   else if(sector==7) {fec=4; mfec=3;}
    //   else if(sector==8) {fec=4; mfec=4;}

    // } else if(!positive && !inner) { // BmO

    //   if(sector==1)      {fec=6; mfec=1;}
    //   else if(sector==2) {fec=6; mfec=2;}
    //   else if(sector==3) {fec=6; mfec=3;}
    //   else if(sector==4) {fec=6; mfec=4;}
    //   else if(sector==5) {fec=8; mfec=1;}
    //   else if(sector==6) {fec=8; mfec=2;}
    //   else if(sector==7) {fec=8; mfec=3;}
    //   else if(sector==8) {fec=8; mfec=4;}

    // } else if(!positive && inner) { // BmI

    //   if(sector==1)      {fec=5; mfec=1;}
    //   else if(sector==2) {fec=5; mfec=2;}
    //   else if(sector==3) {fec=5; mfec=3;}
    //   else if(sector==4) {fec=5; mfec=4;}
    //   else if(sector==5) {fec=7; mfec=1;}
    //   else if(sector==6) {fec=7; mfec=2;}
    //   else if(sector==7) {fec=7; mfec=3;}
    //   else if(sector==8) {fec=7; mfec=4;}

    // }


    // // Change feds
    // int fedOffset1=0, fedOffset4=0;
    // if(positive && inner) { // BpI
    //   fedOffset1=1200-1;
    //   fedOffset4=1224-1;
    // } else if(positive && !inner) { // BpO
    //   fedOffset1=1212-1;
    //   fedOffset4=1236-1;
    // } else if(!positive && !inner) { // BmO
    //   fedOffset1=1260-1;
    //   fedOffset4=1284-1;
    // } else if(!positive && inner) { // BmI
    //   fedOffset1=1248-1;
    //   fedOffset4=1272-1;
    // } else {
    //   cout<<" unknown shell "<<endl;
    // }
    
    // if(sector==1)      {
    //   if(fed==41)      fed=fedOffset1+1;
    //   else if(fed==42) fed=fedOffset1+2;
    //   else if(fed==43) fed=fedOffset1+3;
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==2) {
    //   if(fed==41)      fed=fedOffset1+4;
    //   else if(fed==42) fed=fedOffset1+5;
    //   else if(fed==43) {fed=fedOffset1+3; fedChan +=24;}
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==3) {
    //   if(fed==41)      fed=fedOffset1+6;
    //   else if(fed==44) fed=fedOffset1+7;
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==4) {
    //   if(fed==41)      fed=fedOffset1+8;
    //   else if(fed==42) fed=fedOffset1+9;
    //   else if(fed==43) fed=fedOffset1+10;
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==5) {
    //   if(fed==41)      fed=fedOffset4+8;
    //   else if(fed==42) fed=fedOffset4+9;
    //   else if(fed==43) fed=fedOffset4+10;
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==6) {
    //   if(fed==41)      fed=fedOffset4+6;
    //   else if(fed==44) fed=fedOffset4+7;
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==7) {
    //   if(fed==41)      fed=fedOffset4+4;
    //   else if(fed==42) fed=fedOffset4+5;
    //   else if(fed==43) {fed=fedOffset4+3; fedChan +=24;}
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else if(sector==8) {
    //   if(fed==41)      fed=fedOffset4+1;
    //   else if(fed==42) fed=fedOffset4+2;
    //   else if(fed==43) fed=fedOffset4+3;
    //   else cout<<" unknown "<<fed<<" "<<sector<<endl;
    // } else {
    //   cout<<" unknown sector "<<sector<<endl;
    // }

    // Shift fed channels for sectors 3&6 where some POHs are missing.
    //if( (fed==42)   && (fedChan>=9) )   {cout<<" change "<<endl; fed=44; fedChan=fedChan-8; if(fedChan>40) cout<<" something wrong"<<endl;}
    //if( (fed==43)   && (fedChan<=16) ) {cout<<" change "<<endl; fed=44; fedChan=fedChan+32;}
    // redefine fed channel
    //if(fec==1) fec=5;
    //if(fec==2) fec=6;
    // redefine the FED ID
    //int tmp = linkNum[fed][fedChan-1];
    //if(tmp<1 || tmp>36) cout<<"error in fed channel "<<fed<<" "<<fedChan<<" "<<tmp<<endl;
    //fedChan=tmp;

//     string::size_type idx;
//     bool half=false;
//     idx = name.find("H_MOD");  // half detector 
//     if(idx != string::npos) {half = true;}
//     int layer=0;
//     idx = name.find("_LYR1_");
//     if(idx != string::npos) {layer=1;}
//     idx = name.find("_LYR2_");
//     if(idx != string::npos) {layer=2;}
//     idx = name.find("_LYR3_");
//     if(idx != string::npos) {layer=3;}

//     int sector=0;
//     idx = name.find("_SEC4_");
//     if(idx != string::npos) {sector=4;}
//     idx = name.find("_SEC5_");
//     if(idx != string::npos) {sector=5;}
//     idx = name.find("_SEC8_");
//     if(idx != string::npos) {sector=8;}
    //if(inner) cout<<" Inner ";
    //else cout<<" Outer ";
    //if(positive) cout<<" Positive ";
    //else cout<<" Negative ";
    //if(half) cout<<" Half ";
    //else cout<<" Full ";
    //cout<<" Module "<<endl;

    //tbm="A";
    //if(port>1 && rocOrder<8) tbm="B";
//       idx = name.find("_ROC0");
//       if(idx != string::npos) {
// 	cout<<" ROC0 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC8");
//       }
//       idx = name.find("_ROC1");
//       if(idx != string::npos) {
// 	cout<<" ROC1 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC9");
//       }
//       idx = name.find("_ROC2");
//       if(idx != string::npos) {
// 	cout<<" ROC2 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC10");
//       }
//       idx = name.find("_ROC3");
//       if(idx != string::npos) {
// 	cout<<" ROC3 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC11");
//       }
//       idx = name.find("_ROC4");
//       if(idx != string::npos) {
// 	cout<<" ROC4 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC12");
//       }
//       idx = name.find("_ROC5");
//       if(idx != string::npos) {
// 	cout<<" ROC5 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC13");
//       }
//       idx = name.find("_ROC6");
//       if(idx != string::npos) {
// 	cout<<" ROC6 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC14");
//       }
//       idx = name.find("_ROC7");
//       if(idx != string::npos) {
// 	cout<<" ROC7 "<<idx<<endl;
// 	name.replace(idx+1,idx+4,"ROC15");
//       }
      
//     }

    if(print) 
      cout<<name<<" "<<tbm<<" "<<fec<<"    "<<mfec<<"       "<<mfecChan<<"          "
	  <<hub<<"        "<<port<<"      "<<rocId<<"    "<<fed<<"     "
	  <<fedChan<<"     "<<rocOrder<<endl;

    outfile<<name<<" "<<tbm<<" "<<fec<<"    "<<mfec<<"       "<<mfecChan<<"          "
	   <<hub<<"        "<<port<<"      "<<rocId<<"    "<<fed<<"     "
	   <<fedChan<<"     "<<rocOrder<<endl;

    


  }



//   // Converting interger to string is not trivial.
//   // Either one has to use the C-style sprintf
//   // ot fo the following.
//   string s4 = "test";
//   stringstream str;
//   str << s4 << '.' << 4096; 

//   cout << str<< endl;  // just the pointer to string
//   cout << str.str() << endl; // correct
//   string s5 = str.str(); //assign
//   cout << s5<< endl;
//   string s6;
//   str>>s6;  // assign
//   cout << s6<< endl;
 
  /*
  char name[20] = "RUN";
  char postfix[5] = ".DAT";
  char name1[20], name2[20];
  ret = sprintf(cs,"%d",i); 
  printf(" %d %d %s\n",i,ret,cs);

  len = strlen(name);
  printf(" %d %s\n",len,name);

  strcat(name,cs);

  len = strlen(name);
  printf(" %d %s\n",len,name);

  strcat(name,postfix);

  len = strlen(name);
  printf(" %d %s\n",len,name);

  for(i=0;i<20;i++) {
    printf(" %c ",name[i]);
    if(name[i] == '\0')
      printf("<0-%d>",i);
    else if(name[i]=='\t')
      printf("<t>");
    else if(name[i]=='\n')
      printf("<n>");
    else if(name[i]==' ')
      printf("space");
  }

  printf("\n");
  */

  return(0);
    
}		    


