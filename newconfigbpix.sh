#!/usr/bin/env zsh
# from Jory
# cc0 copyleft
# public domain

# Specify the quadrant, filename appendix (e.g. 1234 or nothing), AND
# sector to start with and sectors to loop over below (change 4 lines!)


# Specify the quadrant:

./newconfig.sh bmo
./newconfig.sh bmi
./newconfig.sh bpi
./newconfig.sh bpo

for settings in 'portcardmap' 'detectconfig' 'translation'
    do
        cardname=${settings}.dat_b
        newcardname=${cardname}pix
        directory=${settings}/0/ # assuming version 0

        echo ${directory}${newcardname}
        cat ${directory}${cardname}mo >  ${directory}${newcardname} # start with 1st specified sector to include any headers

        # now exclude all headers
        grep -v '#' ${directory}${cardname}mi | grep -v -i 'Rocs'  >>  ${directory}${newcardname}
        grep -v '#' ${directory}${cardname}po | grep -v -i 'Rocs'  >>  ${directory}${newcardname}
        grep -v '#' ${directory}${cardname}pi | grep -v -i 'Rocs'  >>  ${directory}${newcardname}

    done

echo ''
echo Did not take into account amc! Sorry not yet implemented
./selectshell.sh bpix