#!/usr/bin/python

import sys,os

version_amc13=0
version_portcardmap=0
version_nametranslation=0
version_detconfig=0

def parseCommandLine(argvector):
    if len(argvector)!=2:
        print("Use selectSector sectorname (e.g. bpi1)")
        exit()

    sector=argvector[1]

    if not (len(sector)==4 or len(sector)==7):
        print("Sector name should have 4 character (e.g. bpi4)!")
        exit()

    if not sector[0]=="b":
        print("Sector should start with a 'b'!")
        exit()

    if not (sector[1]=="p" or sector[1]=="m"):
        print("Sector should contain 'p' or 'm'")
        exit()

    if not (sector[2]=="i" or sector[2]=="o"):
        print("Sector should contain 'i' or 'o'")
        exit()

    if not ((int(sector[3])>=1 and int(sector[3])<=8) or sector[3]=='1234' or sector[3]=='5678'):
        print("Sector should run from 1 to 8 or be 1234 or 5678")
        exit()

    return(sector)


def translateAMC13crate(sector):
    translationUp = {"bmi" : "crate5",
                     "bmo" : "crate6",
                     "bpi" : "crate1",
                     "bpo" : "crate2"}

    translationDo = {"bmi" : "crate7",
                     "bmo" : "crate8",
                     "bpi" : "crate3",
                     "bpo" : "crate4"}
     

    if ((int(sector[3])>=1 and int(sector[3])<=4) or sector[3]=='1234'):
        return(translationUp[sector[0:3]])
    else:
        return(translationDo[sector[0:3]])

def findConfigBase():
    configurationbase=""
    try:
        configurationbase = os.environ['PIXELCONFIGURATIONBASE']
    except:
        print("You have not initialized the POS environment! Please source the environment script of your choice!")
        exit()

    print("You work on PIXELCONFIGURATIONBASE="+configurationbase)
    return configurationbase
    



sector=parseCommandLine(sys.argv)
configurationbase=findConfigBase()

default_amc13=configurationbase+"/amc13/"+str(version_amc13)+"/amc13.dat"
default_portcardmap=configurationbase+"/portcardmap/"+str(version_portcardmap)+"/portcardmap.dat"
default_nametranslation=configurationbase+"/nametranslation/"+str(version_nametranslation)+"/translation.dat"
default_detconfig=configurationbase+"/detconfig/"+str(version_detconfig)+"/detectconfig.dat"

command_amc13="cp "+default_amc13+"_"+translateAMC13crate(sector)+" "+default_amc13
command_portcardmap="cp "+default_portcardmap+"_"+sector+" "+default_portcardmap
command_nametranslation="cp "+default_nametranslation+"_"+sector+" "+default_nametranslation
command_detconfig="cp "+default_detconfig+"_"+sector+" "+default_detconfig


print(command_amc13)
os.system(command_amc13)
print(command_portcardmap)
os.system(command_portcardmap)
print(command_nametranslation)
os.system(command_nametranslation)
print(command_detconfig)
os.system(command_detconfig)

print("Don't forget to to restart POS after this change")
