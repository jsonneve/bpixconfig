#!/usr/bin/env zsh
# from Jory
# cc0 copyleft
# public domain

# Specify the quadrant, filename appendix (e.g. 1234 or nothing), AND
# sector to start with and sectors to loop over below (change 4 lines!)


# Specify the quadrant:
quadrant=bpo # sorry one at a time for now. Use newconfig_complete.sh to create complete config
extensionname='5678' # specify extension name for file, e.g. hello for detectconfig.dat_bmohello
range=(5 6 7 8) # specify which sectors to use

if [[ -n $1 ]]
    quadrant=$1
echo creating new config for $quadrant

for settings in 'portcardmap' 'detectconfig' 'translation'
    do
        cardname=${settings}.dat_${quadrant}
        directory=${settings}/0/ # assuming version 0

        # Name to save the new card to
        newcard=${cardname}${extensionname}

        echo ${directory}${newcard}
        cat ${directory}${cardname}$range[1,1] >  ${directory}${newcard} # start with 1st specified sector to include any headers

        if [[ "$settings"  = 'portcardmap' && $range[1] = 1 ]]
            then
                # Add space for portcardmap sec1 (otherwise missing new line) -- Check!
                # echo '' >> ${directory}${newcard}
        fi
        for i in $range[2,-1]
            do
                # now exclude all headers
                grep -v '#' ${directory}${cardname}${i} | grep -v -i 'Rocs'  >>  ${directory}${newcard}
            done
    done

echo ''
echo Did not take into account amc! Sorry not yet implemented