/** \class MyConvert to parse translation.dat files (nametranslation)
Works from Fed id and channel number to modue name
Works from full module name with ROC number included to full information
Needs still to be extended to work with module names (no ROC)
 */

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <iomanip>

namespace {
  //const bool printErrors  = true;
}

using namespace std;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Class to get names
class MyConvert {
public:
  MyConvert() {}
  ~MyConvert() {}
  static string moduleNameFromFedChan(int fed,int fedChan, string & tbm);
  static bool moduleFedChanFromName(string name, int & fed, int & fedChan, string & tbm);
private:
};

// Method returns the module name and the tbm type as strings
// input: int fed, fedChan
// output: string name, tbm ("A" or "B") 
bool MyConvert::moduleFedChanFromName(string name0, int & fed0,int & fedChan0, string & tbm0) {
  bool status = false;
  //cout<<name0<<endl;

  ifstream infile; //input file, name data_file uniqe
  infile.open("translation.dat",ios::in); // open data file

  //cout << infile.eof() << " " << infile.bad() << " "
  //   << infile.fail() << " " << infile.good()<<endl;

  if (infile.fail()) {
    cout << " File not found " << endl;
    return(" "); // signal error
  }

  //string line;
  char line[500];
  infile.getline(line,500,'\n');

  string name, modName=" ";
  int fec,mfec,mfecChan,hub,port,rocId,fed,fedChan,rocOrder;
  string tbm = " ";
  bool found = false;

  for(int i=0;i<100000;++i) {
    //bool print = false;

    infile>>name;
    //cout<<name<<" "<<name.substr(0,4)<<endl;
    if(name.substr(0,4)=="FPix") {
      //cout<<" fpix "<<endl;
      infile>>fec>>mfec>>mfecChan>>hub>>port>>rocId>>fed>>fedChan>>rocOrder;
      //cout<<fec<<" "<<mfec<<" "<<mfecChan<<" "<<hub<<" "<<port<<" "<<rocId<<" "<<fed<<" "<<fedChan<<" "<<rocOrder<<endl;
    } else if(name.substr(0,4)=="BPix") {
      //cout<<" bpix "<<endl;
      infile>>tbm>>fec>>mfec>>mfecChan>>hub>>port>>rocId>>fed>>fedChan>>rocOrder;
      //cout<<tbm<<" "<<fec<<" "<<mfec<<" "<<mfecChan<<" "<<hub<<" "<<port<<" "<<rocId<<" "<<fed<<" "<<fedChan<<" "<<rocOrder<<endl;
    } else {
      if(name==" ") continue;
    }

    if ( infile.eof() != 0 ) {
      cout<< " end of file " << endl;
      break;;
    } else if (infile.fail()) { // check for errors

      cout << "Cannot read data file " << name << " "<<name.substr(0,4)<<endl;
      return(" ");
    }

    //continue;

    //cout<<name<<" "<<name0<<endl;
    if(name!=name0) continue;

    found = true;
    tbm0=tbm;
    fed0=fed;
    fedChan0=fedChan;

    cout<<" FEC= "<<fec<<" nFEC= "<<mfec<<" mFECChan= "<<mfecChan<<" HUB= "<<hub<<" Port= "<<port<<" ROCid= "<<rocId
	<<" FED= "<<fed<<" FEDChan= "<<fedChan<<" ROCorder= "<<rocOrder<<" TBM= "<<tbm<<endl;

    string::size_type idx;
    idx = name.find("_ROC");
    if(idx != string::npos) {
      //      cout<<" ROC0 "<<idx<<endl;
      //name.replace(idx,idx+4,"     ");
      modName = name.substr(0,(idx));
    }
    
    status=true;
    break;

  }  // end line loop
  
  infile.close();  
  if(!found) cout<<" Module not found "<<fed0<<" "<<fedChan0<<endl;




  return status;

}
string MyConvert::moduleNameFromFedChan(int fed0,int fedChan0, string & tbm0) {
  if(fed0<0 || fed0>39) return " ";
  if(fedChan0<1 || fedChan0>36) return " ";

  ifstream infile; //input file, name data_file uniqe
  infile.open("translation.dat",ios::in); // open data file

  //cout << infile.eof() << " " << infile.bad() << " "
  //   << infile.fail() << " " << infile.good()<<endl;

  if (infile.fail()) {
    cout << " File not found " << endl;
    return(" "); // signal error
  }

  //string line;
  char line[500];
  infile.getline(line,500,'\n');

  string name, modName=" ";
  int fec,mfec,mfecChan,hub,port,rocId,fed,fedChan,rocOrder;
  string tbm = " ";
  int fedOld=-1, fedChanOld=-1;
  bool found = false;
  for(int i=0;i<100000;++i) {
    //bool print = false;

    infile>>name;
    //cout<<name<<" "<<name.substr(0,4)<<endl;
    if(name.substr(0,4)=="FPix") {
      //cout<<" fpix "<<endl;
      infile>>fec>>mfec>>mfecChan>>hub>>port>>rocId>>fed>>fedChan>>rocOrder;
      //cout<<fec<<" "<<mfec<<" "<<mfecChan<<" "<<hub<<" "<<port<<" "<<rocId<<" "<<fed<<" "<<fedChan<<" "<<rocOrder<<endl;
    } else if(name.substr(0,4)=="BPix") {
      //cout<<" bpix "<<endl;
      infile>>tbm>>fec>>mfec>>mfecChan>>hub>>port>>rocId>>fed>>fedChan>>rocOrder;
      //cout<<tbm<<" "<<fec<<" "<<mfec<<" "<<mfecChan<<" "<<hub<<" "<<port<<" "<<rocId<<" "<<fed<<" "<<fedChan<<" "<<rocOrder<<endl;
    } else {
      if(name==" ") continue;
    }

    if ( infile.eof() != 0 ) {
      cout<< " end of file " << endl;
      break;;
    } else if (infile.fail()) { // check for errors

      cout << "Cannot read data file " << name << " "<<name.substr(0,4)<<endl;
      return(" ");
    }

    //continue;
    
    if(fed==fedOld && fedChanOld==fedChan) continue;
    fedOld = fed;
    fedChanOld = fedChan;


    if(fed==fed0 && fedChan==fedChan0) {  // found
      found = true;
      tbm0=tbm;

      string::size_type idx;
      idx = name.find("_ROC");
      if(idx != string::npos) {
        //      cout<<" ROC0 "<<idx<<endl;
        //name.replace(idx,idx+4,"     ");
        modName = name.substr(0,(idx));
      }


      break;
    }
  }  // end line loop
  
  infile.close();  
  if(!found) cout<<" Module not found "<<fed0<<" "<<fedChan0<<endl;

  return modName;

}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Main program
int main(int argc, char **argv) {

  bool error = false;
  bool halt = false;
  int mode = 0; // 0 - fed to name, 1- name to fed

  char * filename;
  if(argc>1) {
    //filename = argv[1];
    mode = atoi(argv[1]);
    if(mode!=1 && mode!=0) mode = 0; 
  }

  int fed = 0, channel = 0;
  string name;

  // Get the module name and tbm type 
  string modName = " ",tbm=" ";

  cout<<mode<<endl;
  if(mode==0) {
    cout<<" enter fed and channel number "<<endl;
    cin>>fed>>channel;
    modName = MyConvert::moduleNameFromFedChan(fed,channel,tbm);
    //int realRocNum = roc;
    //if(tbm=="B") realRocNum = roc + 8; // shift for TBM-N
    cout<<" Fed = "<<setw(3)<<fed<<" "<<", Chan = "<<setw(3)<<channel<<", Module = "<<setw(30)<<modName<<", TBM "<<tbm<<endl;
  } else {
    cout<<" enter module name (with ROC) "<<endl;
    cin>>name;
    bool status = MyConvert::moduleFedChanFromName(name,fed,channel,tbm);
    if(!status) cout<<" Module/ROC not found"<<endl;
  }

  return 0;
}
 
