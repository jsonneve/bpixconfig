#!/usr/bin/env zsh
# from Jory

cardname=portcardmap.dat
directory=portcardmap/0/

#cardname=detectconfig.dat
#directory=detconfig/0/

#cardname=translation.dat
#directory=nametranslation/0/

#newcard=${cardname}5678jory
newcard=${cardname}jory

#cat ${directory}${cardname}5 > ${directory}${newcard}
#echo '' >> ${directory}$newcard

#for i in {6..8}
#    do
#        grep -v '#' ${directory}${cardname}${i} | grep -v -i 'Rocs'  >>  ${directory}${cardname}5678jory
#        #echo '' >> $newportcard
#    done

grep -i -e '#' -e 'Rocs' ${directory}${cardname}_bmi1 >  ${directory}${newcard}
#cat ${directory}${cardname}1 > ${directory}${newcard}
#echo '' >> ${directory}$newcard # only needed for portcardmap of bmi5 (no new line at end of file)
for sector in bpi bpo bmi bmo
    for i in {1..8}
        do
            grep -v '#' ${directory}${cardname}_${sector}${i} | grep -v -i 'Rocs'  >>  ${directory}${newcard}
            #echo '' >> ${directory}$newportcard
        done