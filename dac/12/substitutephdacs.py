import glob
import os
import sys

fileNames = glob.glob('*LYR1*')
for fileName in fileNames:
    with open(fileName, 'r') as file:
        lines = file.readlines()
    with open('../1/' + fileName, 'r') as file2:
        lines2 = file2.readlines()
    newlines = []
    for i,line in enumerate(lines):
        line2 = lines2[i]
        if ('PHScale' in line or 'PHOffset' in line) and ('PHScale' in line2 or 'PHOffset' in line2):
            newlines.append(lines2[i])
        elif ('PHScale' in line or 'PHOffset' in line):
            print "ERROR:", lines[i], lines2[i]
        else:
            newlines.append(lines[i])
    #print newlines
    #raw_input()
    with open(fileName, 'w') as file:
        file.write(''.join(newlines))
    print fileName